# network reconstruction of simple function - x^2, x^3, sin(x) etc

import sys
import random
import math
import numpy
import theano
import theano.tensor as T
import matplotlib.pyplot as plotter

sys.path.append('/home/agrin/Install/grinlib')
from layer import HiddenLayer

theano.config.compute_test_value = 'warn'

# FUNCTION = lambda(x): x ** 2
FUNCTION = lambda(x): math.sin(x * 20)
# FUNCTION = lambda(x): x
BOTTOM_BOUNDARY = 0
TOP_BOUNDARY = 10

RANDOM_SEED = 42
BATCH_SIZE = 10
HIDDEN_LAYER_SIZE = 10

LEARNING_RATE = 0.05
NUMBER_OF_EPOCHS = 1500

if __name__ == '__main__':
  list_of_samples = numpy.arange(0, 1, 0.001)
  list_of_labels = [FUNCTION(sample) for sample in list_of_samples]
  random.seed(RANDOM_SEED)
  data = zip(list_of_samples, list_of_labels)
  random.shuffle(data)
  list_of_samples = [element[0] for element in data]
  list_of_labels = [element[1] for element in data]
  # print(list_of_samples[:10])
  # print(list_of_labels[:10])

  train_samples = numpy.array(list_of_samples[:-100],
                              dtype=theano.config.floatX).reshape(-1, 1)
  train_labels = numpy.array(list_of_labels[:-100],
                              dtype=theano.config.floatX).reshape(-1, 1)
  validation_samples = numpy.array(list_of_samples[-100:],
                                   dtype=theano.config.floatX).reshape(-1, 1)
  validation_labels = numpy.array(list_of_labels[-100:],
                                  dtype=theano.config.floatX).reshape(-1, 1)

  shared_train_samples = theano.shared(train_samples)
  shared_train_labels = theano.shared(train_labels)
  shared_validation_samples = theano.shared(validation_samples)
  shared_validation_labels = theano.shared(validation_labels)

  # print(shared_train_samples.get_value().shape)
  # print(shared_train_labels.get_value().shape)
  # print(shared_validation_samples.get_value().shape)
  # print(shared_validation_labels.get_value().shape)
  # sys.exit()

  random_number_generator = numpy.random.RandomState(RANDOM_SEED)
  minibatch_index = T.lscalar('minibatch_index')
  minibatch_index.tag.test_value = 0
  network_input = T.matrix('network_input')
  network_input.tag.test_value = numpy.zeros((90, 1),
                                             dtype=theano.config.floatX)
  network_output = T.matrix('network_output')
  network_output.tag.test_value = numpy.zeros((90, 1),
                                              dtype=theano.config.floatX)

  first_layer = HiddenLayer(random_number_generator,
                            symbolic_input=network_input,
                            number_of_inputs=1, number_of_outputs=HIDDEN_LAYER_SIZE)

  second_layer = HiddenLayer(random_number_generator,
                            symbolic_input=first_layer.output,
                            number_of_inputs=HIDDEN_LAYER_SIZE,
                            number_of_outputs=HIDDEN_LAYER_SIZE)

  output_layer = HiddenLayer(random_number_generator,
                            symbolic_input=second_layer.output,
                            activation=None, # important - it's output layer
                            number_of_inputs=HIDDEN_LAYER_SIZE, number_of_outputs=1)

  cost = output_layer.mean_squared_error(network_output)
  # theano.printing.debugprint(output_layer.output)
  # sys.exit()

  validation_model = theano.function(
     [minibatch_index],
      output_layer.mean_squared_error(network_output),
      givens={
        network_input: shared_validation_samples[
                                minibatch_index * BATCH_SIZE:
                                (minibatch_index + 1) * BATCH_SIZE],
        network_output: shared_validation_labels[
                                minibatch_index * BATCH_SIZE:
                                (minibatch_index + 1) * BATCH_SIZE]})

  parameters = output_layer.parameters + first_layer.parameters
  gradients = T.grad(cost, parameters)

  updates = []
  for parameter, gradient in zip(parameters, gradients):
    updates.append((parameter, parameter - LEARNING_RATE * gradient))
  # print(len(updates))
  # sys.exit()

  train_model = theano.function([minibatch_index], cost, updates=updates,
         givens={network_input: shared_train_samples[
                                minibatch_index * BATCH_SIZE:
                                (minibatch_index + 1) * BATCH_SIZE],
       network_output: shared_train_labels[
                                minibatch_index * BATCH_SIZE:
                                (minibatch_index + 1) * BATCH_SIZE]})


  print('Training started')
  train_batches_number = shared_train_samples.get_value(borrow=True).shape[0] / BATCH_SIZE
  validation_batches_number = (shared_validation_samples.get_value(borrow=True).shape[0]
                                                                                      / BATCH_SIZE)
  validation_frequency = train_batches_number

  prediction_model = theano.function([minibatch_index], output_layer.output,
                                givens={network_input: shared_validation_samples[
                                                             minibatch_index * BATCH_SIZE:
                                                             (minibatch_index + 1) * BATCH_SIZE]})

  for epoch in xrange(NUMBER_OF_EPOCHS):
    for minibatch_index in xrange(train_batches_number):
      iterator = epoch * train_batches_number + minibatch_index
      current_cost = train_model(minibatch_index)
      if iterator % validation_frequency == 0:
        validation_losses = [validation_model(i) for i in xrange(validation_batches_number)]
        current_validation_loss = numpy.mean(validation_losses)
        print('epoch {}, validation error {}'.format(epoch, current_validation_loss))
    if epoch % 50 == 0:
      list_of_predictions = []
      for i in xrange(validation_batches_number):
        new_prediction = prediction_model(i)
        list_of_predictions.extend(new_prediction)
      for point in zip(shared_validation_samples.get_value(), list_of_predictions):
        plotter.scatter(point[0], point[1])
      plotter.savefig('net_approx' + str(epoch) + '.jpg')
      plotter.clf()
