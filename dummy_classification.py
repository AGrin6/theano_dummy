# network classification of simple cases

import sys
import random
import numpy
import theano
import theano.tensor as T

sys.path.append('/home/agrin/Install/grinlib')
from layer import HiddenLayer
from regression import LogisticRegression

# theano.config.compute_test_value = 'warn'

RANDOM_SEED = 42
BATCH_SIZE = 10
LEARNING_RATE = 0.05
NUMBER_OF_EPOCHS = 1000
HIDDEN_LAYER_SIZE = 20

if __name__ == '__main__':
  # generating data
  list_of_samples = numpy.arange(0, 1, 0.001)
  list_of_labels = [float(sample < 0.2 or sample > 0.8 or (sample > 0.4 and sample < 0.6))
                    for sample in list_of_samples]
  # list_of_labels = [float(sample < 0.3 or sample > 0.7) for sample in list_of_samples]
  random.seed(RANDOM_SEED)
  data = zip(list_of_samples, list_of_labels)
  random.shuffle(data)
  # print(data[:10])
  list_of_samples = [element[0] for element in data]
  list_of_labels = [element[1] for element in data]
  # print(list_of_samples[:10])
  # print(list_of_labels[:10])

  train_samples = numpy.array(list_of_samples[:-100],
                              dtype=theano.config.floatX).reshape(-1, 1)
  train_labels = numpy.array(list_of_labels[:-100],
                             dtype=theano.config.floatX)
  validation_samples = numpy.array(list_of_samples[-100:],
                                   dtype=theano.config.floatX).reshape(-1, 1)
  validation_labels = numpy.array(list_of_labels[-100:],
                                  dtype=theano.config.floatX)

  shared_train_samples = theano.shared(train_samples)
  shared_train_labels = theano.shared(train_labels)
  shared_validation_samples = theano.shared(validation_samples)
  shared_validation_labels = theano.shared(validation_labels)

  random_number_generator = numpy.random.RandomState(RANDOM_SEED)
  minibatch_index = T.lscalar()
  network_input = T.matrix('network_input')
  # network_input.tag.test_value = numpy.zeros((90, 1),
  #                                            dtype=theano.config.floatX)
  network_output = T.ivector('network_output')
  # network_output.tag.test_value = numpy.zeros((90,),
  #                                            dtype=theano.config.floatX)

  # print(shared_validation_samples.get_value().shape)
  # print(shared_validation_labels.get_value().shape)

  first_layer = HiddenLayer(random_number_generator,
                            symbolic_input=network_input,
                            number_of_inputs=1,
                            number_of_outputs=HIDDEN_LAYER_SIZE)

  # second_layer = HiddenLayer(random_number_generator,
  #                            symbolic_input=first_layer.output,
  #                            number_of_inputs=HIDDEN_LAYER_SIZE,
  #                            number_of_outputs=HIDDEN_LAYER_SIZE)

  # output_layer = LogisticRegression(symbolic_input=second_layer.output,
  output_layer = LogisticRegression(symbolic_input=first_layer.output,
                                    number_of_inputs=HIDDEN_LAYER_SIZE,
                                    number_of_outputs=2)

  cost = output_layer.negative_log_likelihood(network_output)

  batch_size_var = T.lscalar()
  validation_model = theano.function(
     [minibatch_index, batch_size_var],
     output_layer.errors(network_output),
     givens={
       network_input: shared_validation_samples[
                                minibatch_index * batch_size_var:
                                (minibatch_index + 1) * batch_size_var],
       network_output: T.cast(shared_validation_labels[
                                minibatch_index * batch_size_var:
                                (minibatch_index + 1) * batch_size_var], 'int32')})
                                   
  # samples_var = T.matrix()
  # labels_var = T.ivector()
  # validation_model2 = theano.function(
  #    [samples_var, labels_var],
  #    output_layer.errors(network_output),
  #    givens={
  #      network_input: samples_var,
  #      network_output: labels_var})
                                   
  parameters = output_layer.parameters + first_layer.parameters
  # parameters = output_layer.parameters + second_layer.parameters + first_layer.parameters
  gradients = T.grad(cost, parameters)

  updates = []
  for parameter, gradient in zip(parameters, gradients):
    updates.append((parameter, parameter - LEARNING_RATE * gradient))

  train_model = theano.function([minibatch_index], cost, updates=updates,
         givens={network_input: shared_train_samples[
                                minibatch_index * BATCH_SIZE:
                                (minibatch_index + 1) * BATCH_SIZE],
       network_output: T.cast(shared_train_labels[
                                minibatch_index * BATCH_SIZE:
                                (minibatch_index + 1) * BATCH_SIZE], 'int32')})

  print('Training started')
  improvement_threshold = 0.995
  train_batches_number = (
             shared_train_samples.get_value(borrow=True).shape[0] / BATCH_SIZE)
  validation_batches_number = (
        shared_validation_samples.get_value(borrow=True).shape[0] / BATCH_SIZE)
  validation_frequency = train_batches_number
  best_parameters = None
  best_validation_loss = numpy.inf
  best_iteration = 0
  best_score = 0.
  
  epoch = 0

  while (epoch < NUMBER_OF_EPOCHS):
    epoch += 1
    for minibatch_index in xrange(train_batches_number):
      iterator = (epoch - 1) * train_batches_number + minibatch_index
      current_cost = train_model(minibatch_index)
      if (iterator + 1) % validation_frequency == 0:
        validation_losses = [validation_model(i, BATCH_SIZE) for i
                                          in xrange(validation_batches_number)]
        # validation_losses2 = [validation_model2(
        #               shared_validation_samples.get_value()[i * BATCH_SIZE:
        #                                         (i + 1) * BATCH_SIZE],
        #                    numpy.asarray(shared_validation_labels.get_value()[
        #                         i * BATCH_SIZE:
        #                         (i + 1) * BATCH_SIZE], dtype=numpy.int32)) for i in xrange(validation_batches_number)]
        current_validation_loss = numpy.mean(validation_losses)
        # current_validation_loss2 = numpy.mean(validation_losses)
        print('epoch {}, validation error {}'
                                       .format(epoch, current_validation_loss))
        # print(current_validation_loss2)
